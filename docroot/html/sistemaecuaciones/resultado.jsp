<%@ include file="init.jsp" %>

<%
	String backUrl = GetterUtil.get(request.getParameter("backUrl"), "");
	Double resultX = GetterUtil.getDouble(request.getParameter("resultX"), 0.0);
	Double resultY = GetterUtil.getDouble(request.getParameter("resultY"), 0.0);
	Double resultZ = GetterUtil.getDouble(request.getParameter("resultZ"), 0.0);
	Object[] results = {resultX, resultY, resultZ};
%>

<liferay-ui:header title="com.piensaenbinario.sistemaecuaciones.title" backURL="<%=backUrl %>"/>

<liferay-ui:error key="error" message="com.piensaenbinario.sistemaecuaciones.error" />
<liferay-ui:success key="success" message="com.piensaenbinario.sistemaecuaciones.success" />

<label>
	<liferay-ui:message key="com.piensaenbinario.sistemaecuaciones.result"  arguments="<%=results %>"/>
</label> 