<%@ include file="init.jsp" %>

<liferay-ui:header title="com.piensaenbinario.sistemaecuaciones.title" />

<portlet:actionURL name="calculaSolucion" var="calculaSolucionUrl" />

<aui:form method="POST" action="<%=calculaSolucionUrl %>" name="fm">
	<aui:input type="hidden" name="backUrl" value="<%=currentUrl %>"/>
	<aui:field-wrapper label="com.piensaenbinario.sistemaecuaciones.first" inlineLabel="true">
		<aui:input label="" name="x1" type="text" inlineField="true" >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>x + </label>
		<aui:input label="" name="y1" type="text" inlineField="true" >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>y + </label>
		<aui:input label="" name="z1" type="text" inlineField="true"  >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>z = </label>
		<aui:input label="" name="val1" type="text" inlineField="true"  >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
	</aui:field-wrapper>
	<aui:field-wrapper label="com.piensaenbinario.sistemaecuaciones.second" inlineField="true" inlineLabel="true">
		<aui:input label="" name="x2" type="text" >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>x + </label>
		<aui:input label="" name="y2" type="text" inlineField="true"  >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>y + </label>
		<aui:input label="" name="z2" type="text" inlineField="true"  >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>z = </label>
		<aui:input label="" name="val2" type="text" inlineField="true"  >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
	</aui:field-wrapper>
	<aui:field-wrapper label="com.piensaenbinario.sistemaecuaciones.third" inlineLabel="true">
		<aui:input label="" name="x3" type="text" inlineField="true"  >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>x + </label>
		<aui:input label="" name="y3" type="text" inlineField="true"  >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>y + </label>
		<aui:input label="" name="z3" type="text" inlineField="true"  >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
		<label>z = </label>
		<aui:input label="" name="val3" type="text" >
			<aui:validator name="required" />
			<aui:validator name="number" />
		</aui:input>
	</aui:field-wrapper>
	<aui:button value="com.piensaenbinario.sistemaecuaciones.calculate" type="submit" id="button"/>
</aui:form>