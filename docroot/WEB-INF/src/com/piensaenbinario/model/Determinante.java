package com.piensaenbinario.model;

public class Determinante {

	private double a11;
	private double a12;
	private double a13;
	private double a21;
	private double a22;
	private double a23;
	private double a31;
	private double a32;
	private double a33;

	public double getA11() {
		return a11;
	}
	public void setA11(double a11) {
		this.a11 = a11;
	}
	public double getA12() {
		return a12;
	}
	public void setA12(double a12) {
		this.a12 = a12;
	}
	public double getA13() {
		return a13;
	}
	public void setA13(double a13) {
		this.a13 = a13;
	}
	public double getA21() {
		return a21;
	}
	public void setA21(double a21) {
		this.a21 = a21;
	}
	public double getA22() {
		return a22;
	}
	public void setA22(double a22) {
		this.a22 = a22;
	}
	public double getA23() {
		return a23;
	}
	public void setA23(double a23) {
		this.a23 = a23;
	}
	public double getA31() {
		return a31;
	}
	public void setA31(double a31) {
		this.a31 = a31;
	}
	public double getA32() {
		return a32;
	}
	public void setA32(double a32) {
		this.a32 = a32;
	}
	public double getA33() {
		return a33;
	}
	public void setA33(double a33) {
		this.a33 = a33;
	}
	
}
