package com.piensaenbinario;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.piensaenbinario.model.Determinante;

/**
 * Portlet implementation class SistemaEcuacionesPortlet
 */
public class SistemaEcuacionesPortlet extends MVCPortlet {
 
	private static Log _log = LogFactoryUtil.getLog(SistemaEcuacionesPortlet.class);
	
	/**
	 * Action del portlet que calcula la solucion del sistema
	 * y lo muestra en la pagina resultado.jsp
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 * @throws SystemException 
	 */
	public void calculaSolucion(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws SystemException{
		
		//Inicializamos variables
		double resultX = 0.0;
		double resultY = 0.0;
		double resultZ = 0.0;
		
		//Valores de la primera ecuaci�n
		Double x1 = GetterUtil.getDouble(actionRequest.getParameter("x1"));
		Double y1 = GetterUtil.getDouble(actionRequest.getParameter("y1"));
		Double z1 = GetterUtil.getDouble(actionRequest.getParameter("z1"));
		Double val1 = GetterUtil.getDouble(actionRequest.getParameter("val1"));
		_log.info("1a ecuaci�n con valores de x:"+x1+"; y:"+y1+"; z:"+z1+"; valor:"+val1);
		
		//Valores de la segunda ecuaci�n
		Double x2 = GetterUtil.getDouble(actionRequest.getParameter("x2"));
		Double y2 = GetterUtil.getDouble(actionRequest.getParameter("y2"));
		Double z2 = GetterUtil.getDouble(actionRequest.getParameter("z2"));
		Double val2 = GetterUtil.getDouble(actionRequest.getParameter("val2"));
		_log.info("2a ecuaci�n con valores de x:"+x2+"; y:"+y2+"; z:"+z2+"; valor:"+val2);
		
		//Valores de la tercera ecuaci�n
		Double x3 = GetterUtil.getDouble(actionRequest.getParameter("x3"));
		Double y3 = GetterUtil.getDouble(actionRequest.getParameter("y3"));
		Double z3 = GetterUtil.getDouble(actionRequest.getParameter("z3"));
		Double val3 = GetterUtil.getDouble(actionRequest.getParameter("val3"));
		_log.info("3a ecuaci�n con valores de x:"+x3+"; y:"+y3+"; z:"+z3+"; valor:"+val3);

		//Obtenemos los determinantes en funci�n de los valores
		Determinante dx = new Determinante();
		Determinante dy = new Determinante();
		Determinante dz = new Determinante();
		Determinante dnum = new Determinante();
		createDeterminants(x1, x2, x3, y1, y2, y3, z1, z2, z3, val1, val2, val3, dx, dy, dz, dnum);
		
		//Calculamos los resultados para cada incognita
		double denominador = calDet(dnum);
		if(denominador!=0){
			resultX = calDet(dx)/denominador;
			resultY = calDet(dy)/denominador;
			resultZ = calDet(dz)/denominador;
			SessionMessages.add(actionRequest, "success");
			_log.info("Soluciones x="+resultX+" ;y="+resultY+" ;z="+resultZ);
		}else{
			SessionErrors.add(actionRequest, "error");
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest)+SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			_log.info("No existe soluci�n para el sistema de ecuaciones, denomiador 0");
		}
		
		actionResponse.setRenderParameter("resultX", String.valueOf(resultX));
		actionResponse.setRenderParameter("resultY", String.valueOf(resultY));
		actionResponse.setRenderParameter("resultZ", String.valueOf(resultZ));
		actionResponse.setRenderParameter("jspPage", "/html/sistemaecuaciones/resultado.jsp");
		actionResponse.setRenderParameter("backUrl", actionRequest.getParameter("backUrl"));
	}
	
	/**
	 * A partir de un sistema de ecuaciones, crea los 4 determinantes
	 * necesarios para la resoluci�n del sistema de ecuaciones
	 * 
	 * @param x1
	 * @param x2
	 * @param x3
	 * @param y1
	 * @param y2
	 * @param y3
	 * @param z1
	 * @param z2
	 * @param z3
	 * @param val1
	 * @param val2
	 * @param val3
	 * @param dx
	 * @param dy
	 * @param dz
	 * @param dnum
	 */
	public void createDeterminants(double x1, double x2, double x3, double y1, double y2, 
			double y3, double z1, double z2, double z3, double val1, double val2, double val3,
			Determinante dx, Determinante dy,	Determinante dz, Determinante dnum){
		
		//Creamos el determinante de x
		dx.setA11(val1);
		dx.setA12(y1);
		dx.setA13(z1);
		dx.setA21(val2);
		dx.setA22(y2);
		dx.setA23(z2);
		dx.setA31(val3);
		dx.setA32(y3); 
		dx.setA33(z3);
		
		//Creamos el determinante de y
		dy.setA11(x1);
		dy.setA12(val1);
		dy.setA13(z1);
		dy.setA21(x2);
		dy.setA22(val2);
		dy.setA23(z2);
		dy.setA31(x3);
		dy.setA32(val3); 
		dy.setA33(z3);
		
		//Creamos el determinante de z
		dz.setA11(x1);
		dz.setA12(y1);
		dz.setA13(val1);
		dz.setA21(x2);
		dz.setA22(y2);
		dz.setA23(val2);
		dz.setA31(x3);
		dz.setA32(y3); 
		dz.setA33(val3);
		
		//Creamos el determinante del numerador
		dnum.setA11(x1);
		dnum.setA12(y1);
		dnum.setA13(z1);
		dnum.setA21(x2);
		dnum.setA22(y2);
		dnum.setA23(z2);
		dnum.setA31(x3);
		dnum.setA32(y3); 
		dnum.setA33(z3);
		
	}
	
	/**
	 * Calcula el valor de un determinante
	 * 
	 * @param determinant
	 * @return double
	 */
	public double calDet(Determinante determinant){
		
		return determinant.getA11()*determinant.getA22()*determinant.getA33() + 
				determinant.getA12()*determinant.getA23()*determinant.getA31() + 
				determinant.getA13()*determinant.getA21()*determinant.getA32() - 
				determinant.getA13()*determinant.getA22()*determinant.getA31() - 
				determinant.getA12()*determinant.getA21()*determinant.getA33() - 
				determinant.getA11()*determinant.getA23()*determinant.getA32();
	}
}
