import static org.junit.Assert.assertEquals;
import com.piensaenbinario.SistemaEcuacionesPortlet;
import com.piensaenbinario.model.Determinante;

import org.junit.Test;

public class UnitTest {

	@Test
    public void calDet(){
		
		SistemaEcuacionesPortlet sisEcu = new SistemaEcuacionesPortlet();
		Determinante det = new Determinante(); 
		det.setA11(12);
		det.setA12(4);
		det.setA13(5);
		det.setA21(4);
		det.setA22(1);
		det.setA23(-2);
		det.setA31(3);
		det.setA32(2); 
		det.setA33(4);
		assertEquals("Determinante con valor", 33.0, sisEcu.calDet(det), 0.0);
		det.setA11(1);
		det.setA12(1);
		det.setA13(1);
		det.setA21(1);
		det.setA22(1);
		det.setA23(1);
		det.setA31(3);
		det.setA32(2); 
		det.setA33(4);
		assertEquals("Determinante linealmente dependiente", 0, sisEcu.calDet(det), 0.0);
    }
}